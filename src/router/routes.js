
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'onboard', component: () => import('pages/onboard/onboard.vue') },
      { path: '@:account', component: () => import('pages/profile/profile.vue'), meta: { auth: true } },
      { path: 'profiles', component: () => import('pages/profiles/profiles.vue'), meta: { auth: true } },
      { path: 'balances', component: () => import('pages/balances/balances.vue'), meta: { auth: true } },
      { path: 'transfer', component: () => import('pages/transfer/transfer.vue'), meta: { auth: true } },
      { path: 'proposals', component: () => import('pages/proposals/proposals.vue'), meta: { auth: true } },
      { path: 'proposals/:id', component: () => import('pages/proposal/proposal.vue'), meta: { auth: true } },
      { path: 'verifyuser', component: () => import('pages/verify-user/verify-user.vue'), meta: { auth: true } },
      { path: 'assets', component: () => import('pages/assets/list/assets-list.vue'), meta: { auth: true } },
      { path: 'assets/add', component: () => import('pages/assets/add/asset-add.vue'), meta: { auth: true } },
      { path: 'assets/:id', component: () => import('pages/assets/view/asset-view.vue'), meta: { auth: true } },
      // Refactor
      { path: 'landing', component: () => import('pages/Index.vue') },
      { path: 'trade', component: () => import('pages/Trade.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
