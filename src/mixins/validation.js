import { mapActions } from 'vuex'

export const validation = {
  data () {
    return {
      rules: {
        accountExists: async account => (await this.isAccountFree(account.toLowerCase())) || 'This account is already taken',
        accountFormat: val => /^([a-z]|[1-5]|.){1,12}$/.test(val.toLowerCase()) || 'The account must contain lowercase characters only, number from 1 to 5 or a period.',
        accountFormatBasic: val => /^([a-z]|[1-5]){12}$/.test(val.toLowerCase()) || 'The account must contain lowercase characters only and number from 1 to 5',
        accountLength: val => val.length <= 12 || 'The account must contain 12 characters',
        required: val => !!val || 'This field is required',
        requiredIf: cond => val => (cond && !!val) || 'This field is required',
        assetFormat: val => /^(([0-9]*)|(([0-9]*)\.([0-9]*)))$/.test(val) || 'Amount must be numeric. Do not include symbol',
        assetPositive: val => parseFloat(val) > 0 || 'Amount must be greater than 0',
        lessThanOrEqual: qty => val => parseFloat(val) <= qty || `The field must be less than or equal to ${qty}`
      }
    }
  },
  methods: {
    ...mapActions('account', ['isAccountFree']),
    async validate (form) {
      if (!form) return true
      let valid = true
      for await (const key of Object.keys(form)) {
        if (Array.isArray(form[key])) {
          for (let i = 0; i < form[key].length; i += 1) {
            for await (const subKey of Object.keys(form[key][i])) {
              valid = await this.$refs[`${key}${i}_${subKey}`][0].validate() && valid
            }
          }
        } else {
          if (this.$refs[key]) {
            valid = await this.$refs[key].validate() && valid
          }
        }
      }
      return valid
    },
    async resetValidation (form) {
      await this.$nextTick()
      if (!form) return
      for (const key of Object.keys(form)) {
        if (Array.isArray(form[key])) {
          for (let i = 0; i < form[key].length; i += 1) {
            for (const subKey of Object.keys(form[key][i])) {
              this.$refs[`${key}${i}_${subKey}`][0].resetValidation()
            }
          }
        } else {
          if (this.$refs[key]) {
            this.$refs[key].resetValidation()
          }
        }
      }
    }
  }
}
