export const sendSmsOtp = async function ({ commit }, smsNumber) {
  try {
    const result = await this.$axios.post('/send-sms-otp',
      {
        smsNumber
      }
    )
    if (result.success) {
      commit('setPhoneNumber', smsNumber)
    }

    return result.success
  } catch (e) {
    this.$sentry.captureException(e)
    return false
  }
}

export const verifyOtp = async function ({ commit, state, rootState }, { code, phoneNumber, newUser }) {
  const data = {
    smsNumber: state.phoneNumber || phoneNumber,
    smsOtp: code
  }
  if (newUser) {
    data.gyftieAccount = newUser.account
    data.activeKey = newUser.activeKey
    data.ownerKey = newUser.ownerKey
  } else {
    data.gyftieAccount = rootState.account.account
  }
  try {
    const result = await this.$axios.post('/verify-attach', data)
    return result.success
  } catch (e) {
    this.$sentry.captureException(e)
    return false
  }
}

export const refer = async function ({ commit, state, rootState }, referrer) {
  const actions = [{
    account: process.env.GYFTIECONTRACT,
    name: 'referuser',
    data: {
      referrer,
      account_to_refer: rootState.account.account
    }
  }]

  return this.$api.signTransaction(actions)
}

export const claim = async function ({ commit, state, rootState }, claimKey) {
  const actions = [{
    account: process.env.GYFTIECONTRACT,
    name: 'claim',
    data: {
      account: rootState.account.account,
      claim_key: claimKey
    }
  }]

  return this.$api.signTransaction(actions)
}

export const isBadgeHolder = async function (context, { account, badge }) {
  try {
    const result = await this.$api.getTableRows({
      code: process.env.GYFTIECONTRACT,
      scope: process.env.GYFTIECONTRACT,
      table: 'badgeaccts',
      index_position: 2,
      key_type: 'i64',
      lower_bound: account,
      upper_bound: account,
      limit: 10
    })
    if (result && result.rows.length > 0) {
      return result.rows.some(b => b.badge_holder === account && b.badge_id === badge)
    }
  } catch (e) {
    this.$sentry.captureException(e)
  }
  return false
}
