export default () => ({
  account: null,
  verified: false,
  loading: '',
  requestAccount: false,
  availableAccounts: {
    list: {
      data: [],
      loaded: true
    }
  }
})
