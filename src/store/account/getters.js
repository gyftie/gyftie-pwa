export const isAuthenticated = ({ account, verified }) => account !== null && verified
export const hasToVerify = ({ account, verified }) => account !== null && !verified
export const account = ({ account }) => account
export const hasAccount = ({ account }) => account !== null
export const loading = ({ loading }) => loading
export const availableAccounts = ({ availableAccounts }) => availableAccounts.list.data
export const availableAccountsLoaded = ({ availableAccounts }) => availableAccounts.list.loaded
export const requestAccount = ({ requestAccount }) => requestAccount
