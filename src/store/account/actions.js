import { Api, JsonRpc } from 'eosjs'
import { JsSignatureProvider } from 'eosjs/dist/eosjs-jssig'

export const loginWallet = async function ({ commit, dispatch }, { idx, account, returnUrl }) {
  const authenticator = this.$ual.authenticators[idx]
  commit('setLoadingWallet', authenticator.getStyle().text)
  await authenticator.init()
  if (!account) {
    const requestAccount = await authenticator.shouldRequestAccountName()
    if (requestAccount) {
      await dispatch('fetchAvailableAccounts', idx)
      commit('setRequestAccount', true)
      return null
    }
  }
  let error
  try {
    const users = await authenticator.login(account)
    if (users.length) {
      account = await users[0].getAccountName()
      commit('setAccount', account)
      this.$ualUser = users[0]
      this.$type = 'ual'
      localStorage.setItem('autoLogin', authenticator.constructor.name)
      localStorage.setItem('account', account)
    }
  } catch (e) {
    error = (authenticator.getError() && authenticator.getError().message) || e.cause.message
  }
  if (account) {
    await dispatch('profiles/verifyProfile', { account, returnUrl }, { root: true })
  }
  commit('setLoadingWallet')
  return error
}

export const loginInApp = async function ({ commit, dispatch }, { account, privateKey, returnUrl }) {
  try {
    const signatureProvider = new JsSignatureProvider([privateKey])
    const rpc = new JsonRpc(`${process.env.NETWORK_PROTOCOL}://${process.env.NETWORK_HOST}:${process.env.NETWORK_PORT}`)
    const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() })

    this.$type = 'inApp'
    this.$inAppUser = api

    commit('setAccount', account)
  } catch (e) {
    console.log(e)
    return 'Invalid private key'
  }
  await dispatch('profiles/verifyProfile', { account, returnUrl }, { root: true })
}

export const lightLogin = async function ({ commit }, { account, privateKey }) {
  try {
    const signatureProvider = new JsSignatureProvider([privateKey])
    const rpc = new JsonRpc(`${process.env.NETWORK_PROTOCOL}://${process.env.NETWORK_HOST}:${process.env.NETWORK_PORT}`)
    const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() })

    this.$type = 'inApp'
    this.$inAppUser = api

    commit('setAccount', account)
  } catch (e) {
    console.log(e)
    return 'Invalid private key'
  }
}

export const logout = async function ({ commit }) {
  if (this.$type === 'ual') {
    const wallet = localStorage.getItem('autoLogin')
    const idx = this.$ual.authenticators.findIndex(auth => auth.constructor.name === wallet)
    this.$ual.authenticators[idx].logout()
    localStorage.removeItem('autoLogin')
  }

  commit('clearAccount')
  this.$ualUser = null
  this.$inAppUser = null
  this.$type = null
  this.$router.push({ path: '/' })
}

export const autoLogin = async function ({ dispatch, commit }, returnUrl) {
  const wallet = localStorage.getItem('autoLogin')
  const idx = this.$ual.authenticators.findIndex(auth => auth.constructor.name === wallet)
  if (idx !== -1) {
    const authenticator = this.$ual.authenticators[idx]
    await authenticator.init()
    await dispatch('loginWallet', { idx, returnUrl, account: localStorage.getItem('account') })
  }
}

export const isAccountFree = async function (context, accountName) {
  try {
    await this.$accountApi.get(accountName)
    return false
  } catch (e) {
    // Catch the 404 error if the account doesn't exist
    return true
  }
}

export const fetchAvailableAccounts = async function ({ commit }, idx) {
  commit('resetAvailableAccounts')
  const chainId = process.env.NETWORK_CHAIN_ID
  const authenticator = this.$ual.authenticators[idx]
  const map = await authenticator.getAccountNamesPerChain()
  const accounts = map.has(chainId) ? map.get(chainId) : []
  commit('setAvailableAccounts', accounts)
}
