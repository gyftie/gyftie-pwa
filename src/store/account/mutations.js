export const setLoadingWallet = (state, wallet) => {
  state.loading = wallet
}

export const setAccount = function (state, account) {
  state.account = account
}

export const setVerified = (state, verified) => {
  state.verified = verified
}

export const clearAccount = function (state) {
  localStorage.removeItem('autoLogin')
  state.account = null
  state.verified = false
  this.$router.push({ path: '/' })
}

export const setRequestAccount = (state, requestAccount) => {
  state.requestAccount = requestAccount
}

export const resetAvailableAccounts = (state) => {
  state.availableAccounts.list.data = []
  state.availableAccounts.list.loaded = false
}

export const setAvailableAccounts = (state, accounts) => {
  state.availableAccounts.list.data = accounts
  state.availableAccounts.list.loaded = true
}
