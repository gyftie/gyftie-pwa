export const fetchProposals = async function ({ commit, state }) {
  const result = await this.$api.getTableRows({
    code: process.env.GYFTIECONTRACT,
    scope: process.env.GYFTIECONTRACT,
    table: 'proposals',
    lower_bound: state.proposals.length ? state.proposals[state.proposals.length - 1].proposal_id : '',
    limit: state.pagination.limit
  })

  commit('addProposals', result)
}

export const fetchProposal = async function (context, id) {
  const result = await this.$api.getTableRows({
    code: process.env.GYFTIECONTRACT,
    scope: process.env.GYFTIECONTRACT,
    table: 'proposals',
    lower_bound: id,
    upper_bound: id,
    limit: 1
  })
  return result.rows[0]
}

export const submitProposal = async function ({ rootState }, { title, content }) {
  const actions = [{
    account: process.env.GYFTIECONTRACT,
    name: 'propose',
    data: {
      proposer: rootState.account.account,
      proposal_name: title,
      notes: content
    }
  }]

  return this.$api.signTransaction(actions)
}

export const voteFor = async function ({ rootState }, payload) {
  const actions = [{
    account: process.env.GYFTIECONTRACT,
    name: 'votefor',
    data: {
      voter: rootState.account.account,
      proposal_id: payload
    }
  }]

  return this.$api.signTransaction(actions)
}

export const voteAgainst = async function ({ rootState }, payload) {
  const actions = [{
    account: process.env.GYFTIECONTRACT,
    name: 'voteagainst',
    data: {
      voter: rootState.account.account,
      proposal_id: payload
    }
  }]

  return this.$api.signTransaction(actions)
}

export const unVote = async function ({ rootState }, payload) {
  const actions = [{
    account: process.env.GYFTIECONTRACT,
    name: 'unvoteprop',
    data: {
      voter: rootState.account.account,
      proposal_id: payload
    }
  }]

  return this.$api.signTransaction(actions)
}
