export const addProposals = (state, { rows, more }) => {
  if (rows) {
    // Remove the first item as it's the lower_bound
    const arr = state.proposals.length ? rows.slice(1) : rows
    state.proposals = state.proposals.concat(arr)
  }
  state.proposalsLoaded = !more
}
