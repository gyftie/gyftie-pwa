export const verifyProfile = async function ({ commit }, { account, returnUrl }) {
  const result = await this.$api.getTableRows({
    code: process.env.GYFTIECONTRACT,
    scope: process.env.GYFTIECONTRACT,
    table: 'profiles2',
    lower_bound: account,
    upper_bound: account,
    limit: 1
  })
  if (result.rows.length) {
    commit('account/setVerified', true, { root: true })
    this.$router.push({ path: returnUrl || `/@${account}` })
  } else {
    this.$router.push({ path: '/onboard' })
  }
}

export const isProfileCreated = async function ({ commit, rootState }) {
  const result = await this.$api.getTableRows({
    code: process.env.GYFTIECONTRACT,
    scope: process.env.GYFTIECONTRACT,
    table: 'profiles2',
    lower_bound: rootState.account.account,
    upper_bound: rootState.account.account,
    limit: 1
  })
  if (result.rows.length) {
    commit('account/setVerified', true, { root: true })
    return true
  } else {
    return false
  }
}

export const fetchProfiles = async function ({ commit, state }) {
  const options = {
    code: process.env.GYFTIECONTRACT,
    scope: process.env.GYFTIECONTRACT,
    table: 'profiles2',
    limit: state.pagination.limit,
    reverse: state.pagination.reverse
  }

  if (state.pagination.order === 'rank' || (state.search && !Number.isNaN(parseInt(state.search)))) {
    options.index_position = 2
    options.key_type = 'i64'
  }
  if (!state.search) {
    options.lower_bound = state.profiles.length ? state.profiles[state.profiles.length - 1][state.pagination.order] : ''
  } else {
    if (Number.isNaN(parseInt(state.search))) {
      // Account search
      options.lower_bound = state.search
      options.upper_bound = state.search
    } else {
      // Rank search
      options.lower_bound = parseInt(state.search)
      options.upper_bound = parseInt(state.search)
      options.limit = 1000
    }
  }

  const result = await this.$api.getTableRows(options)
  commit('addProfiles', result)
}

export const toggleVoteForUser = async function ({ commit, rootState }, { profile, dir }) {
  const voter = rootState.account.account
  const actions = [{
    account: process.env.GYFTIECONTRACT,
    name: dir ? 'voteforuser' : 'unvoteuser',
    data: {
      voter,
      profile
    }
  }]

  const result = await this.$api.signTransaction(actions)
  if (result) {
    commit('toggleVote', { voter, profile, dir })
  }
}

export const fetchProfile = async function (context, accountName) {
  const profile = await this.$api.getTableRows({
    code: process.env.GYFTIECONTRACT,
    scope: process.env.GYFTIECONTRACT,
    table: 'profiles2',
    limit: 1,
    lower_bound: accountName,
    upper_bound: accountName
  })

  const badges = await this.$api.getTableRows({
    code: process.env.GYFTIECONTRACT,
    scope: process.env.GYFTIECONTRACT,
    table: 'badges'
  })

  const accountBadges = await this.$api.getTableRows({
    code: process.env.GYFTIECONTRACT,
    scope: process.env.GYFTIECONTRACT,
    table: 'badgeaccts',
    lower_bound: accountName,
    index_position: 2,
    key_type: 'i64'
  })

  return {
    profile: profile.rows[0],
    badges: badges.rows,
    accountBadges: accountBadges.rows
  }
}

export const profileExists = async function (context, accountName) {
  const profile = await this.$api.getTableRows({
    code: process.env.GYFTIECONTRACT,
    scope: process.env.GYFTIECONTRACT,
    table: 'profiles2',
    limit: 1,
    lower_bound: accountName,
    upper_bound: accountName
  })
  return profile.rows.length > 0
}

const sha256 = async message => {
  const msgUint8 = new TextEncoder('utf-8').encode(message) // encode as UTF-8
  const hashBuffer = await crypto.subtle.digest('SHA-256', msgUint8) // hash the message
  const hashArray = Array.from(new Uint8Array(hashBuffer)) // convert hash to byte array
  return hashArray.map(b => ('00' + b.toString(16)).slice(-2)).join('') // convert bytes to hex string
}

const makeHash = async (id, expiration) => {
  const data = id.substr(0, 2) +
    expiration.substr(0, 2) +
    id.substr(2, 4) +
    expiration.substr(3, 5) +
    id.substr(4, 6) +
    expiration.substr(6, 10) +
    id.substr(6, id.length)
  return sha256(data)
}

export const verifyUser = async function ({ commit, state, rootState }, form) {
  let expiration = '2050/01/01'
  if (!form.noExpirationDate) {
    const year = form.expirationDate.slice(6, 10)
    const month = form.expirationDate.slice(0, 2)
    const day = form.expirationDate.slice(3, 5)
    expiration = `${year}/${month}/${day}`
  }

  const actions = [{
    account: process.env.GYFTIECONTRACT,
    name: 'addhash',
    data: {
      idchecker: rootState.account.account,
      idholder: form.account,
      idhash: await makeHash(form.id, expiration),
      idexpiration: expiration,
      idtype: form.type,
      idnotes: form.typeOther || ''
    }
  }]

  return this.$api.signTransaction(actions)
}
