export const addProfiles = (state, { rows, more }) => {
  if (rows) {
    // Remove the first item as it's the lower_bound
    const arr = state.profiles.length ? rows.slice(1) : rows
    state.profiles = state.profiles.concat(arr)
  }
  state.profilesLoaded = !more
}

export const setSorting = (state, { order, reverse }) => {
  state.pagination = {
    ...state.pagination,
    order,
    reverse
  }
  state.profiles = []
  state.profilesLoaded = false
}

export const setSearch = (state, search) => {
  state.search = search
  state.pagination = {
    ...state.pagination,
    order: 'account',
    reverse: false
  }
  state.profiles = []
  state.profilesLoaded = false
}

export const toggleVote = (state, { voter, profile, dir }) => {
  const idx = state.profiles.findIndex(p => p.account === profile)
  const user = state.profiles[idx]
  if (dir) {
    user.promotion_votes_for_this_profile.push(voter)
  } else {
    user.promotion_votes_for_this_profile = user.promotion_votes_for_this_profile.filter(v => v !== voter)
  }

  state.profiles = [
    ...state.profiles.slice(0, idx),
    user,
    ...state.profiles.slice(idx + 1)
  ]
}
