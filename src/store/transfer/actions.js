export const fetchAvailableLiquidities = async function ({ rootState }) {
  let liquidGft
  let liquidEos
  let result = await this.$api.getTableRows({
    code: process.env.GYFTIECONTRACT,
    scope: process.env.GYFTIECONTRACT,
    table: 'profiles2',
    lower_bound: rootState.account.account,
    upper_bound: rootState.account.account,
    limit: 1
  })
  if (result.rows.length) {
    liquidGft = result.rows[0].gft_balance || 0
  }

  result = await this.$api.getTableRows({
    code: 'eosio.token',
    scope: rootState.account.account,
    table: 'accounts',
    limit: 1
  })

  if (result.rows.length) {
    liquidEos = result.rows[0].balance || 0
  } else {
    liquidEos = 0
  }

  return {
    liquidGft,
    liquidEos
  }
}

export const transfer = async function ({ commit, state, rootState }, { to, quantity, token, memo }) {
  const actions = [{
    account: token === 'EOS' ? 'eosio.token' : process.env.GYFTIECONTRACT,
    name: 'transfer',
    data: {
      from: rootState.account.account,
      to,
      quantity: `${parseFloat(quantity).toFixed(token === 'EOS' ? 4 : 8)} ${token}`,
      memo
    }
  }]

  return this.$api.signTransaction(actions)
}
