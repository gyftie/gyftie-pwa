export const assets = ({ assets }) => assets.list.data
export const assetsLoaded = ({ assets }) => assets.list.loaded
export const asset = ({ assets }) => assets.view.asset
export const assetBalance = ({ assets }) => assets.view.balance
