export const resetAssets = (state) => {
  state.assets.list.data = []
  state.assets.list.loaded = false
}

export const addAssets = (state, { rows, more }) => {
  if (rows) {
    // Remove the first item as it's the lower_bound
    const arr = state.assets.list.data.length ? rows.slice(1) : rows
    state.assets.list.data = state.assets.list.data.concat(arr)
  }
  state.assets.list.loaded = !more
}

export const setAsset = (state, asset) => {
  state.assets.view.asset = asset
}

export const setAssetBalance = (state, obj) => {
  state.assets.view.balance = obj && obj.balance
}
