export const fetchAssets = async function ({ commit, state }) {
  const result = await this.$api.getTableRows({
    code: process.env.ASSETSCONTRACT,
    scope: process.env.ASSETSCONTRACT,
    table: 'assets',
    limit: state.assets.list.pagination.limit
  })
  commit('addAssets', result)
}

export const fetchAsset = async function (context, id) {
  const result = await this.$api.getTableRows({
    code: process.env.ASSETSCONTRACT,
    scope: process.env.ASSETSCONTRACT,
    table: 'assets',
    limit: 1,
    lower_bound: id,
    upper_bound: id
  })
  return result.rows[0]
}

export const createAsset = async function ({ rootState }, form) {
  const actions = [{
    account: process.env.ASSETSCONTRACT,
    name: 'create',
    data: {
      creator: rootState.account.account,
      asset_name: form.name,
      asset_description: form.description,
      asset_category: form.category,
      asset_subcategory: form.subcategory,
      asset_symbol: `0,${form.symbol}`,
      attribute_pairs: form.immutableAttributes
    }
  }]

  return this.$api.signTransaction(actions)
}

export const setAttributes = async function (context, { id, attributes }) {
  const actions = [{
    account: process.env.ASSETSCONTRACT,
    name: 'setattrs',
    data: {
      asset_id: id,
      attributes
    }
  }]

  return this.$api.signTransaction(actions)
}

export const fetchAccounts = async function (context, user) {
  const result = await this.$api.getTableRows({
    code: process.env.ASSETSCONTRACT,
    scope: user,
    table: 'accounts',
    limit: 100
  })
  return result.rows
}

export const fetchAccountsBalance = async function ({ commit }, { account, symbol }) {
  const result = await this.$api.getTableRows({
    code: process.env.ASSETSCONTRACT,
    scope: account,
    table: 'accounts',
    limit: 1000
  })
  commit('setAssetBalance', result.rows.find(r => r.balance.replace(/[0-9.\s]/g, '') === symbol))
}

export const buy = async function ({ rootState }, payload) {
  const actions = [
    {
      account: process.env.GYFTIECONTRACT,
      name: 'transfer',
      data: {
        from: rootState.account.account,
        to: process.env.ASSETSCONTRACT,
        quantity: `${parseFloat(payload.tlosgft).toFixed(8)} TLOSGFT`,
        memo: 'Buy asset funding'
      }
    },
    {
      account: process.env.ASSETSCONTRACT,
      name: 'buy',
      data: {
        buyer: rootState.account.account,
        tlosgft_to_spend: `${parseFloat(payload.tlosgft).toFixed(8)} TLOSGFT`,
        symbol_to_buy: payload.symbol
      }
    }
  ]

  return this.$api.signTransaction(actions)
}

export const sell = async function ({ rootState }, payload) {
  const actions = [{
    account: process.env.ASSETSCONTRACT,
    name: 'sell',
    data: {
      seller: rootState.account.account,
      amount_to_sell: payload.amount
    }
  }]

  return this.$api.signTransaction(actions)
}
