export default () => ({
  assets: {
    list: {
      data: [],
      loaded: false,
      pagination: {
        limit: 100
      }
    },
    view: {
      asset: null,
      balance: '0 SYMBOL'
    }
  }
})
