export const fetchBalances = async function ({ rootState }) {
  const balances = {
    eosUsd: 0,
    gftEos: 0,
    liquidEos: `0.000 ${this.networkToken}`,
    gftOnOrder: `0.00000000 ${this.gyftieToken}`,
    eosOnOrder: `0.0000 ${this.networkToken}`,
    liquidGft: `0.00000000 ${this.gyftieToken}`,
    unstakingGft: `0.00000000 ${this.gyftieToken}`,
    stakedGft: `0.00000000 ${this.gyftieToken}`
  }

  const counterPart = this.$currencyId === 'eos' ? 'eos' : 'telos'
  const data = await this.$gecko.simple.price({
    ids: [counterPart],
    vs_currencies: ['usd']
  })

  balances.eosUsd = data.data[counterPart].usd

  let result
  result = await this.$api.getTableRows({
    code: process.env.GFTORDERBOOK,
    scope: process.env.GFTORDERBOOK,
    table: 'states',
    limit: 1
  })
  if (result.rows.length) {
    balances.gftEos = result.rows[0].last_price
  }
  result = await this.$api.getTableRows({
    code: 'eosio.token',
    scope: rootState.account.account,
    table: 'accounts',
    limit: 1
  })
  if (result.rows.length) {
    balances.liquidEos = result.rows[0].balance
  }
  result = await this.$api.getTableRows({
    code: process.env.GFTORDERBOOK,
    scope: rootState.account.account,
    table: 'balances',
    limit: 10
  })
  const gftBalanceRow = result.rows.filter(el => el.token_contract === process.env.GYFTIECONTRACT)
  if (gftBalanceRow.length) {
    balances.gftOnOrder = gftBalanceRow[0].funds
  }

  const eosBalanceRow = result.rows.filter(el => el.token_contract === 'eosio.token')
  if (eosBalanceRow.length) {
    balances.eosOnOrder = eosBalanceRow[0].funds
  }

  result = await this.$api.getTableRows({
    code: process.env.GYFTIECONTRACT,
    scope: process.env.GYFTIECONTRACT,
    table: 'profiles2',
    lower_bound: rootState.account.account,
    upper_bound: rootState.account.account,
    limit: 1
  })
  const profile = result.rows[0]

  // GFT balances
  if (profile) {
    balances.liquidGft = profile.gft_balance || 0
    balances.unstakingGft = profile.unstaking_balance || 0
    balances.stakedGft = profile.staked_balance || 0
  }

  return balances
}

export const fetchLiquidGft = async function ({ rootState }) {
  const result = await this.$api.getTableRows({
    code: process.env.GYFTIECONTRACT,
    scope: process.env.GYFTIECONTRACT,
    table: 'profiles2',
    lower_bound: rootState.account.account,
    upper_bound: rootState.account.account,
    limit: 1
  })
  const profile = result.rows[0]
  return (profile && profile.gft_balance) || 0
}

export const unstake = async function ({ rootState }) {
  const result = await this.$api.getTableRows({
    code: process.env.GYFTIECONTRACT,
    scope: process.env.GYFTIECONTRACT,
    table: 'profiles2',
    lower_bound: rootState.account.account,
    limit: 1
  })
  const profile = result.rows[0]
  let stakedBalance
  if (profile) {
    stakedBalance = profile.staked_balance
  }
  const actions = [{
    account: process.env.GYFTIECONTRACT,
    name: 'requnstake',
    data: {
      user: rootState.account.account,
      quantity: stakedBalance
    }
  }]

  return this.$api.signTransaction(actions)
}
