import { Notify } from 'quasar'

export const addNotification = (state, { transactionId, actions, error }) => {
  actions.forEach(action => {
    state.notifications = [...state.notifications].concat({
      ...actionsToNotification(action),
      transactionId,
      status: error ? 'error' : 'success',
      error,
      read: false,
      id: Math.round(Math.random() * 10e7)
    })
  })
  if (error === null) {
    state.successCount += actions.length
    Notify.create({
      color: 'green',
      message: 'Transaction success'
    })
  } else {
    state.errorCount += actions.length
    Notify.create({
      color: 'red',
      message: 'Transaction error'
    })
  }
  localStorage.setItem('notifications', JSON.stringify(state.notifications))
}

export const unmarkRead = (state) => {
  state.successCount = 0
  state.errorCount = 0
  const notifs = JSON.parse(localStorage.getItem('notifications')) || []
  notifs.forEach(n => {
    n.read = true
  })
  state.notifications = notifs
  localStorage.setItem('notifications', JSON.stringify(notifs))
}

export const clearNotifications = (state) => {
  state.notifications = []
  state.successCount = 0
  state.errorCount = 0
  localStorage.removeItem('notifications')
}

export const initNotifications = (state) => {
  if (!localStorage) return
  const notifs = localStorage.getItem('notifications')
  if (notifs) {
    state.notifications = JSON.parse(notifs)
    state.successCount = state.notifications.filter(n => !n.read && n.status === 'success').length
    state.errorCount = state.notifications.filter(n => !n.read && n.status === 'error').length
  }
}

const actionsToNotification = action => {
  const actionName = `${action.account}_${action.name}`
  switch (actionName) {
    case `${process.env.ASSETSCONTRACT}_create`:
      return { icon: 'create', title: 'Create an asset', content: action.data.asset_name }
    case `${process.env.ASSETSCONTRACT}_setattrs`:
      return { icon: 'edit_attributes', title: 'Edit asset attributes', content: action.data.asset_id }
    case `${process.env.ASSETSCONTRACT}_buy`:
      return { icon: 'done', title: 'Buy an asset', content: action.data.symbol_to_buy }
    case `${process.env.ASSETSCONTRACT}_sell`:
      return { icon: 'done', title: 'Sell an asset', content: action.data.amount_to_sell }
    case `${process.env.GYFTIECONTRACT}_requnstake`:
      return { icon: 'attach_money', title: 'Unstake', content: action.data.quantity }
    case `${process.env.GYFTIECONTRACT}_referuser`:
      return { icon: 'verified_user', title: 'Refer a user', content: action.data.account_to_refer }
    case `${process.env.GYFTIECONTRACT}_claim`:
      return { icon: 'vpn_key', title: 'Claim account', content: action.data.account }
    case `${process.env.GYFTIECONTRACT}_voteforuser`:
      return { icon: 'how_to_vote', title: 'Vote for a user', content: action.data.profile }
    case `${process.env.GYFTIECONTRACT}_unvoteuser`:
      return { icon: 'how_to_vote', title: 'Unvote a user', content: action.data.profile }
    case `${process.env.GYFTIECONTRACT}_addhash`:
      return { icon: 'verified_user', title: 'Verify a user', content: action.data.idholder }
    case `${process.env.GYFTIECONTRACT}_propose`:
      return { icon: 'send', title: 'Submit a proposal', content: action.data.proposal_name }
    case `${process.env.GYFTIECONTRACT}_votefor`:
      return { icon: 'how_to_vote', title: 'Vote for a proposal', content: action.data.proposal_id }
    case `${process.env.GYFTIECONTRACT}_voteagainst`:
      return { icon: 'how_to_vote', title: 'Vote against a proposal', content: action.data.proposal_id }
    case `${process.env.GYFTIECONTRACT}_unvoteprop`:
      return { icon: 'how_to_vote', title: 'Unvote a proposal', content: action.data.proposal_id }
    case `${process.env.GYFTIECONTRACT}_transfer`:
    case 'eosio.token_transfer':
      return { icon: 'attach_money', title: 'Transfer', content: `${action.data.quantity} from ${action.data.from} to ${action.data.to}` }
    case `${process.env.GFTORDERBOOK}_delbuyorder`:
      return { icon: 'delete', title: 'Delete buy', content: action.data.buyorder_id }
    case `${process.env.GFTORDERBOOK}_delsellorder`:
      return { icon: 'delete', title: 'Delete sell', content: action.data.sellorder_id }
    case `${process.env.OBFA_CONTRACT}_noop`:
      return { icon: 'done', title: 'Free CPU', content: 'Gyftie sponsored cpu' }
    default:
      return { icon: 'rss_feed', title: actionName, content: JSON.stringify(action.data) }
  }
}
