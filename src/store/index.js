import Vue from 'vue'
import Vuex from 'vuex'

import assets from './assets'
import account from './account'
import balances from './balances'
import notifications from './notifications'
import onboard from './onboard'
import profiles from './profiles'
import proposals from './proposals'
import trades from './trades'
import transfer from './transfer'

Vue.use(Vuex)

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      account,
      balances,
      assets,
      notifications,
      onboard,
      profiles,
      proposals,
      trades,
      transfer
    },
    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
