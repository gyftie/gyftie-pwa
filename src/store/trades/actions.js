export const loadBuyOrders = async function ({ rootState }) {
  const orders = {}
  let result = await this.$api.getTableRows({
    code: process.env.GFTORDERBOOK,
    scope: process.env.GFTORDERBOOK,
    table: 'buyorders',
    index_position: 3,
    key_type: 'i64',
    reverse: true
  })
  orders.buyOrders = result.rows

  result = await this.$api.getTableRows({
    code: process.env.GFTORDERBOOK,
    scope: process.env.GFTORDERBOOK,
    table: 'buyorders',
    lower_bound: rootState.account.account,
    index_position: 2,
    key_type: 'i64'
  })
  orders.myBuyOrders = result.rows

  return orders
}

export const loadSellOrders = async function ({ rootState }) {
  const orders = {}
  let result = await this.$api.getTableRows({
    code: process.env.GFTORDERBOOK,
    scope: process.env.GFTORDERBOOK,
    table: 'sellorders',
    index_position: 3,
    key_type: 'i64'
  })
  orders.sellOrders = result.rows

  result = await this.$api.getTableRows({
    code: process.env.GFTORDERBOOK,
    scope: process.env.GFTORDERBOOK,
    table: 'sellorders',
    lower_bound: rootState.account.account,
    index_position: 2,
    key_type: 'i64'
  })
  orders.mySellOrders = result.rows

  return orders
}

export const fetchLiquidTokens = async function ({ rootState }) {
  const balance = {
    liquidEos: '0.0000 EOS',
    liquidGft: '0.00000000 GFT'
  }
  let result = await this.$api.getTableRows({
    code: 'eosio.token',
    scope: rootState.account.account,
    table: 'accounts',
    limit: 1
  })
  if (result.rows.length) {
    balance.liquidEos = result.rows[0].balance
  }

  result = await this.$api.getTableRows({
    code: process.env.GYFTIECONTRACT,
    scope: process.env.GYFTIECONTRACT,
    table: 'profiles2',
    lower_bound: rootState.account.account,
    limit: 1
  })
  const profile = result.rows[0]

  if (profile) {
    balance.liquidGft = profile.gft_balance || 0
  }

  return balance
}

export const marketSell = async function ({ rootState }, { amount }) {
  const actions = [{
    account: process.env.GYFTIECONTRACT,
    name: 'transfer',
    data: {
      from: rootState.account.account,
      to: process.env.GFTORDERBOOK,
      quantity: parseFloat(amount).toFixed(8) + ' GFT',
      memo: 'Transfer for GFT Market Sell Order'
    }
  },
  {
    account: process.env.GFTORDERBOOK,
    name: 'marketsell',
    data: {
      seller: rootState.account.account,
      gft_amount: parseFloat(amount).toFixed(8) + ' GFT'
    }
  }]

  return this.$api.signTransaction(actions)
}

export const limitSell = async function ({ rootState }, { amount, price }) {
  const actions = [
    {
      account: process.env.GYFTIECONTRACT,
      name: 'transfer',
      data: {
        from: rootState.account.account,
        to: process.env.GFTORDERBOOK,
        quantity: parseFloat(amount).toFixed(8) + ' GFT',
        memo: 'Transfer for GFT Sell Order'
      }
    },
    {
      account: process.env.GFTORDERBOOK,
      name: 'limitsellgft',
      data: {
        seller: rootState.account.account,
        price_per_gft: parseFloat(price).toFixed(4) + ' EOS',
        gft_amount: parseFloat(amount).toFixed(8) + ' GFT'
      }
    }]

  return this.$api.signTransaction(actions)
}

export const marketBuy = async function ({ rootState }, { amount }) {
  const actions = [{
    account: 'eosio.token',
    name: 'transfer',
    data: {
      from: rootState.account.account,
      to: process.env.GFTORDERBOOK,
      quantity: parseFloat(amount).toFixed(4) + ' EOS',
      memo: 'Transfer for GFT Market Buy Order'
    }
  },
  { account: process.env.GFTORDERBOOK,
    name: 'marketbuy',
    data: {
      buyer: rootState.account.account,
      eos_amount: parseFloat(amount).toFixed(4) + ' EOS'
    }
  }]

  return this.$api.signTransaction(actions)
}

export const limitBuy = async function ({ rootState }, { amount, price }) {
  const amountToBuy = (parseFloat(amount) * parseFloat(price)).toFixed(8) + ' GFT'
  const actions = [
    {
      account: 'eosio.token',
      name: 'transfer',
      data: {
        from: rootState.account.account,
        to: process.env.GFTORDERBOOK,
        quantity: parseFloat(price).toFixed(4) + ' EOS',
        memo: 'Transfer for GFT Limit Buy Order'
      }
    },
    {
      account: process.env.GFTORDERBOOK,
      name: 'limitbuygft',
      data: {
        buyer: rootState.account.account,
        price_per_gft: parseFloat(price).toFixed(4) + ' EOS',
        gft_amount: amountToBuy
      }
    }]

  return this.$api.signTransaction(actions)
}

export const deleteBuyOrder = async function ({ rootState }, id) {
  const actions = [{
    account: process.env.GFTORDERBOOK,
    name: 'delbuyorder',
    data: {
      buyorder_id: id
    }
  }]

  return this.$api.signTransaction(actions)
}

export const deleteSellOrder = async function ({ rootState }, id) {
  const actions = [{
    account: process.env.GFTORDERBOOK,
    name: 'delsellorder',
    data: {
      sellorder_id: id
    }
  }]

  return this.$api.signTransaction(actions)
}
