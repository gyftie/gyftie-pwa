import axios from 'axios'

export const http = axios.create({
  baseURL: process.env.INTEGRATED_SERVICES_URL,
  headers: {
    'Content-Type': 'application/json',
    'x-api-key': process.env.INTEGRATED_SERVICES_API_KEY
  }
})

http.interceptors.response.use(response => response.data || {})

export const accountApi = axios.create({
  baseURL: process.env.ACCOUNT_API_URL,
  headers: {
    'Content-Type': 'application/json',
    'x-api-key': process.env.ACCOUNT_API_KEY
  }
})

accountApi.interceptors.response.use(response => response.data || {})

export default ({ Vue, store }) => {
  Vue.prototype.$axios = http
  store['$axios'] = http
  Vue.prototype.$accountApi = accountApi
  store['$accountApi'] = accountApi
}
