export default async ({ Vue }) => {
  Vue.mixin({
    computed: {
      networkToken: () => process.env.NETWORK_TOKEN_SYMBOL,
      gyftieToken: () => process.env.GYFTIE_TOKEN_SYMBOL,
      isEOS: () => process.env.NETWORK_TOKEN_SYMBOL === 'EOS',
      isTELOS: () => process.env.NETWORK_TOKEN_SYMBOL === 'TLOS',
      approvers: () => ['danielflora3', 'zombiejigsaw', 'gyftieuser11', 'bitcarbonpa2', 'samchaudhry1', 'zero2pieos12']
    }
  })
}
