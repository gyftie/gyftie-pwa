import { UAL } from 'universal-authenticator-library'
import { EOSIOAuth } from '@smontero/ual-eosio-reference-authenticator'
import { KeycatAuthenticator } from '@smontero/ual-keycat'
import { Lynx } from '@smontero/ual-lynx'
import { Scatter } from 'ual-scatter'
import { Sqrl } from '@smontero/ual-sqrl'
import { TokenPocket } from '@smontero/ual-token-pocket'
import { Anchor } from 'ual-anchor'

export default async ({ Vue, store }) => {
  const {
    NETWORK_CHAIN_ID: chainId,
    NETWORK_PROTOCOL: protocol,
    NETWORK_HOST: host,
    NETWORK_PORT: port
  } = process.env

  const mainChain = {
    chainId,
    rpcEndpoints: [{
      protocol,
      host,
      port
    }]
  }

  const authenticators = [
    new EOSIOAuth([mainChain], { appName: process.env.APP_NAME, protocol: 'eosio' }),
    new Sqrl([mainChain], { appName: process.env.APP_NAME }),
    new Scatter([mainChain], { appName: process.env.APP_NAME }),
    new KeycatAuthenticator([mainChain]),
    new Lynx([mainChain], { endPoint: `${protocol}://${host}:${port}` }),
    new TokenPocket([mainChain]),
    new Anchor([mainChain], { appName: process.env.APP_NAME })
  ]

  const ual = new UAL([mainChain], process.env.APP_NAME, authenticators)
  store['$ual'] = ual
  Vue.prototype.$ual = ual
}
