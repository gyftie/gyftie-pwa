import { Api, JsonRpc } from 'eosjs'

const signTransaction = async function (actions) {
  actions.forEach(action => {
    if (!action.authorization || !action.authorization.length) {
      action.authorization = [{
        actor: this.state.account.account,
        permission: 'active'
      }]
    }
  })
  let transactionId = null
  let error = null
  try {
    const {
      OBFA_CONTRACT: obfaContract,
      OBFA_COSIGNER: cosigner,
      OBFA_COSIGNER_PERMISSION: cosignerPermission
    } = process.env

    actions = [{
      authorization: [{
        actor: cosigner,
        permission: cosignerPermission
      }],
      account: obfaContract,
      name: 'noop',
      data: {
        number: 1
      }
    }].concat(actions)

    if (this.$type === 'ual') {
      const result = await this.$ualUser.signTransaction({
        actions
      }, {
        blocksBehind: 3,
        expireSeconds: 30
      })
      transactionId = result.transactionId
    } else if (this.$type === 'inApp') {
      const result = await this.$inAppUser.transact({
        actions
      }, {
        blocksBehind: 3,
        expireSeconds: 30
      })
      transactionId = result.transaction_id
    }
  } catch (e) {
    if (process.env.NODE_ENV !== 'production') {
      console.log(actions, e, this.$type === 'inApp' ? e.message : e.cause.message)
    }
    error = this.$type === 'inApp' ? e.message : e.cause.message
    this.$sentry.captureException(e)
  }
  this.commit('notifications/addNotification', { transactionId, actions, error }, { root: true })
  return error === null
}

const getTableRows = async function (options) {
  return this.$defaultApi.rpc.get_table_rows({
    json: true,
    ...options
  })
}

export default ({ store }) => {
  const rpc = new JsonRpc(`${process.env.NETWORK_PROTOCOL}://${process.env.NETWORK_HOST}:${process.env.NETWORK_PORT}`)
  store['$defaultApi'] = new Api({ rpc, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() })
  store['$api'] = {
    signTransaction: signTransaction.bind(store),
    getTableRows: getTableRows.bind(store)
  }
}
