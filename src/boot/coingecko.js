import CoinGecko from 'coingecko-api'

export default async ({ Vue, store }) => {
  const gecko = new CoinGecko()
  const currencyId = process.env.NETWORK_TOKEN_SYMBOL.toLowerCase()

  Vue.prototype.$currencyId = currencyId
  Vue.prototype.$gecko = gecko

  store['$currencyId'] = currencyId
  store['$gecko'] = gecko
}
