require('dotenv').config()

const CopyWebpackPlugin = require('copy-webpack-plugin')
const path = require('path')

module.exports = function (ctx) {
  return {
    resolve: {
      alias: {
        '~': path.resolve(__dirname, 'src')
      }
    },
    // app boot file (/src/boot)
    // --> boot files are part of "main.js"
    boot: [
      'axios',
      'network-properties',
      { path: 'sentry', server: false },
      { path: 'ual', server: false },
      { path: 'api', server: false },
      { path: 'coingecko', server: false }
    ],

    css: [
      'app.styl'
    ],

    extras: [
      'fontawesome-v5',
      'roboto-font', // optional, you are not bound to it
      'material-icons' // optional, you are not bound to it
    ],

    framework: {
      iconSet: 'fontawesome-v5',
      // lang: 'de', // Quasar language

      // all: true, // --- includes everything; for dev only!

      components: [
        'QAvatar',
        'QBadge',
        'QBanner',
        'QBtn',
        'QBtnToggle',
        'QCard',
        'QCardActions',
        'QCardSection',
        'QCheckbox',
        'QChip',
        'QDate',
        'QDialog',
        'QDrawer',
        'QHeader',
        'QIcon',
        'QImg',
        'QInfiniteScroll',
        'QInnerLoading',
        'QInput',
        'QItem',
        'QItemLabel',
        'QItemSection',
        'QLayout',
        'QLinearProgress',
        'QList',
        'QMenu',
        'QOptionGroup',
        'QPage',
        'QPageContainer',
        'QPageSticky',
        'QPopupProxy',
        'QRadio',
        'QScrollArea',
        'QSelect',
        'QSeparator',
        'QSpace',
        'QSpinner',
        'QSpinnerDots',
        'QStep',
        'QStepper',
        'QStepperNavigation',
        'QTab',
        'QTabs',
        'QTabPanel',
        'QTabPanels',
        'QToggle',
        'QToolbar',
        'QToolbarTitle',
        'QTooltip',
        'QSeparator',
        'QTable',
        'QTd',
        'QTh',
        'QTr'
      ],

      directives: [
        'ClosePopup',
        'Ripple'
      ],

      // Quasar plugins
      plugins: [
        'Notify'
      ]
    },

    supportIE: false,

    build: {
      env: {
        APP_NAME: process.env.APP_NAME,
        NETWORK_PROTOCOL: process.env.NETWORK_PROTOCOL,
        NETWORK_BLOCKCHAIN: process.env.NETWORK_BLOCKCHAIN,
        NETWORK_HOST: process.env.NETWORK_HOST,
        NETWORK_PORT: process.env.NETWORK_PORT,
        NETWORK_CHAIN_ID: process.env.NETWORK_CHAIN_ID,
        TRX_WEB_PREFIX: process.env.TRX_WEB_PREFIX,
        ACCT_WEB_PREFIX: process.env.ACCT_WEB_PREFIX,
        INTEGRATED_SERVICES_URL: process.env.INTEGRATED_SERVICES_URL,
        INTEGRATED_SERVICES_API_KEY: process.env.INTEGRATED_SERVICES_API_KEY,
        ACCOUNT_API_URL: process.env.ACCOUNT_API_URL,
        ACCOUNT_API_KEY: process.env.ACCOUNT_API_KEY,
        GYFTIECONTRACT: process.env.GYFTIECONTRACT,
        GFTORDERBOOK: process.env.GFTORDERBOOK,
        ASSETSCONTRACT: process.env.ASSETSCONTRACT,
        GYFTIE_PHONE_NUMBER: process.env.GYFTIE_PHONE_NUMBER,
        SENTRY_DSN: process.env.SENTRY_DSN,
        NETWORK_TOKEN_SYMBOL: process.env.NETWORK_TOKEN_SYMBOL,
        GYFTIE_TOKEN_SYMBOL: process.env.GYFTIE_TOKEN_SYMBOL,
        OBFA_CONTRACT: process.env.OBFA_CONTRACT,
        OBFA_COSIGNER: process.env.OBFA_COSIGNER,
        OBFA_COSIGNER_PERMISSION: process.env.OBFA_COSIGNER_PERMISSION
      },
      scopeHoisting: true,
      vueRouterMode: 'history',
      // vueCompiler: true,
      // gzip: true,
      // analyze: true,
      // extractCSS: false,
      extendWebpack (cfg) {
        cfg.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /node_modules/
        })

        cfg.module.rules.push({
          test: /\.pug$/,
          loader: 'pug-plain-loader'
        })

        cfg.plugins.push(new CopyWebpackPlugin(
          [{ from: './src/statics/*.json', to: './', force: true, flatten: true }],
          { copyUnmodified: true }
        ))

        cfg.resolve.alias = {
          ...cfg.resolve.alias,
          '~': path.resolve(__dirname, 'src')
        }
      }
    },

    devServer: {
      // port: 8080,
      open: true // opens browser window automatically
    },

    // animations: 'all', // --- includes all animations
    animations: [],

    ssr: {
      pwa: false
    },

    pwa: {
      workboxOptions: {
        skipWaiting: true,
        clientsClaim: true
      },
      // workboxPluginMode: 'InjectManifest',
      // workboxOptions: {}, // only for NON InjectManifest
      manifest: {
        // name: 'Gyftie',
        // short_name: 'Gyftie',
        // description: 'Financial reputation system designed by collaborative intelligence',
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#027be3',
        icons: [
          {
            'src': 'statics/icons/icon-128x128.png',
            'sizes': '128x128',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-192x192.png',
            'sizes': '192x192',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-256x256.png',
            'sizes': '256x256',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-384x384.png',
            'sizes': '384x384',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-512x512.png',
            'sizes': '512x512',
            'type': 'image/png'
          }
        ]
      }
    },

    cordova: {
      // id: 'org.cordova.quasar.app',
      // noIosLegacyBuildFlag: true, // uncomment only if you know what you are doing
    },

    electron: {
      // bundler: 'builder', // or 'packager'

      extendWebpack (cfg) {
        // do something with Electron main process Webpack cfg
        // chainWebpack also available besides this extendWebpack
      },

      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options

        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',

        // Windows only
        // win32metadata: { ... }
      },

      builder: {
        // https://www.electron.build/configuration/configuration

        // appId: 'gyftie-pwa'
      }
    }
  }
}
